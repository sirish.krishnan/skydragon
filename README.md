                \||/
                |  @___oo
      /\  /\   / (__,,,,|
     ) /^\) ^\/ _)
     )   /^\/   _)
     )   _ /  / _)
 /\  )/\/ ||  | )_)
<  >      |(,,) )__)
 ||      /    \)___)\
 | \____(      )___) )___
  \______(_______;;; __;;;

SkyDragon
#########

SkyDragon is a group balloon mapping project; this
repository contains scripts used for the same.

The script file used on field for taking pics
via the RPiCam & log sensor data is this :
    
    code/tests/skydragon-sensor-poll.py
