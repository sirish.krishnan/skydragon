import picamera
from time import sleep

camera = picamera.PiCamera()
camera.vflip = True
#camera.capture('image.jpg')
camera.annotate_text = "[SkyDragon] : Namaste!"
camera.start_preview()
sleep(2)
camera.annotate_text = "[SkyDragon] : Testing brightness, initially no setting"
sleep(3)
camera.annotate_text = "[SkyDragon]: Brightness = 20"
camera.brightness=20
sleep(3)
camera.annotate_text = "[SkyDragon]: Brightness = 30"
camera.brightness=30
sleep(3)
camera.annotate_text = "[SkyDragon]: Brightness = 40"
camera.brightness=40
sleep(3)
camera.annotate_text = "[SkyDragon]: Brightness = 50"
camera.brightness=50
sleep(3)
camera.annotate_text = "[SkyDragon]: Brightness = 60"
camera.brightness=60
sleep(3)
camera.annotate_text = "[SkyDragon]: Brightness = 70"
camera.brightness=70
sleep(3)
camera.annotate_text = "[SkyDragon]: Brightness = 80"
camera.brightness=80
sleep(3)
camera.annotate_text = "[SkyDragon]: Brightness = 90"
camera.brightness=90
sleep(3)
camera.stop_preview
