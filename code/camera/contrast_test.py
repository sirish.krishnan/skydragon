import picamera
from time import sleep

camera = picamera.PiCamera()
camera.vflip = True
#camera.capture('image.jpg')
camera.annotate_text = "[SkyDragon] : Namaste!"
camera.start_preview()
sleep(2)
camera.annotate_text = "[SkyDragon] : Testing contrast, initially no setting"
sleep(3)
camera.annotate_text = "[SkyDragon]: Contrast = 20"
camera.contrast=20
sleep(3)
camera.annotate_text = "[SkyDragon]: Contrast = 30"
camera.contrast=30
sleep(3)
camera.annotate_text = "[SkyDragon]: Contrast = 40"
camera.contrast=40
sleep(3)
camera.annotate_text = "[SkyDragon]: Contrast = 50"
camera.contrast=50
sleep(3)
camera.annotate_text = "[SkyDragon]: Contrast = 60"
camera.contrast=60
sleep(3)
camera.annotate_text = "[SkyDragon]: Contrast = 70"
camera.contrast=70
sleep(3)
camera.annotate_text = "[SkyDragon]: Contrast = 80"
camera.contrast=80
sleep(3)
camera.annotate_text = "[SkyDragon]: Contrast = 90"
camera.contrast=90
sleep(3)
camera.stop_preview
