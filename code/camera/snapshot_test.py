import picamera
from time import sleep

import sys

if len(sys.argv) == 1:
    print "Too few args, please give the name of the person!"
    sys.exit()
else:
    name = sys.argv[1]
    print "Roger that!"

camera = picamera.PiCamera()
camera.resolution = (2592, 1944)
camera.brightness = 50
camera.vflip = True
camera.start_preview()
camera.annotate_text = "[SkyDragon] : Namaste!"
sleep(1)
camera.annotate_text = "[SkyDragon] : Clicking pic in 5s..!"
sleep(1)
camera.annotate_text = "[SkyDragon] : 5..!"
sleep(1)
camera.annotate_text = "[SkyDragon] : 4..!"
sleep(1)
camera.annotate_text = "[SkyDragon] : 3..!"
sleep(1)
camera.annotate_text = "[SkyDragon] : 2..!"
sleep(1)
camera.annotate_text = "[SkyDragon] : 1..Say Cheese !"
sleep(1)
camera.annotate_text = "[SkyDragon] : Hi " + name + " !"
camera.capture(name + '.jpg')
camera.stop_preview
