import datetime as dt
from picamera import PiCamera
import time 

camera = PiCamera()
#camera.resolution = (1280, 720)
camera.resolution = (1920,1080)
camera.vflip = True
camera.framerate = 30

camera.start_preview()
camera.annotate_text = 'SkyDragon ' + dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
camera.start_recording('video.h264')

start = dt.datetime.now()
while (dt.datetime.now() - start).seconds < 30:
    camera.annotate_text =  'SkyDragon ' + dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    camera.wait_recording(0.2)
camera.stop_recording() 
camera.stop_preview()
