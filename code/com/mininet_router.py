#!/usr/bin/python

"""
router.py: Network with Linux IP router

This script has been adapted from mininet/examples/linuxrouter.py.

This script converts a Node into a router using IP forwarding
already built into Linux.

The topology creates a router and 2 IP subnets:

    - 10.0.0.0/24 (r0-eth1, IP: 10.0.0.1)
    - 10.0.1.0/24 (r0-eth2, IP: 10.0.1.1)

Each subnet consists of a 2 to 3 hosts connected to
a single switch:

    r0-eth1 - s1-eth1 - h1-eth0 (IP: 10.0.0.5)
            - s1-eth2 - h2-eth0 (IP: 10.0.0.6)

    r0-eth2 - s2-eth1 - h3-eth0 (IP: 10.0.1.5)    
              s2-eth2 - h4-eth0 (IP: 10.0.1.6)  
              s2-eth3 - h5-eth0 (IP: 10.0.1.7)  

The script relies on default routing entries that are
automatically created for each router interface, as well
as 'defaultRoute' parameters for the host interfaces.

Additional routes may be added to the router or hosts by
executing 'ip route' or 'route' commands on the router or hosts.
"""

import os
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import Node
from mininet.log import setLogLevel, info
from mininet.cli import CLI

from mininet.node import OVSController

class LinuxRouter( Node ):
    "A Node with IP forwarding enabled."

    def config( self, **params ):
        super( LinuxRouter, self).config( **params )
        # Enable forwarding on the router
        self.cmd( 'sysctl net.ipv4.ip_forward=1' )

    def terminate( self ):
        self.cmd( 'sysctl net.ipv4.ip_forward=0' )
        super( LinuxRouter, self ).terminate()


class NetworkTopo( Topo ):
    "A LinuxRouter connecting two IP subnets"

    def build( self, **_opts ):

        defaultIP = '10.0.0.1/24'  # IP address for r0-eth1
        router = self.addNode( 'r0', cls=LinuxRouter, ip=defaultIP )

        s1, s2 = [ self.addSwitch( s ) for s in 's1', 's2' ]

        self.addLink( s1, router, intfName2='r0-eth1',
                      params2={ 'ip' : defaultIP } )  # for clarity
        self.addLink( s2, router, intfName2='r0-eth2',
                      params2={ 'ip' : '10.0.1.1/24' } )

        h1 = self.addHost( 'h1', ip='10.0.0.5/24', defaultRoute='via 10.0.0.1' )
	h2 = self.addHost( 'h2', ip='10.0.0.6/24', defaultRoute='via 10.0.0.1' )
        h3 = self.addHost( 'h3', ip='10.0.1.5/24', defaultRoute='via 10.0.1.1' )
        h4 = self.addHost( 'h4', ip='10.0.1.6/24', defaultRoute='via 10.0.1.1' )
        h5 = self.addHost( 'h5', ip='10.0.1.7/24', defaultRoute='via 10.0.1.1' )

        for h, s in [ (h1, s1), (h2, s1), (h3,s2), (h4,s2), (h5,s2) ]:
            self.addLink( h, s )


def run():
    "Test linux router"
    topo = NetworkTopo()
    #net = Mininet( topo=topo )  # controller is used by s1-s3
    net = Mininet(topo=topo,controller = OVSController)
    net.start()

    # Start SSHD on all hosts
    # Note : For successful ARP spoofing attack, use 
    # "ssh -1 <hostname>" to start a V1 SSH session
    h1 = net.get('h1')
    h1.cmd('/usr/sbin/sshd -o UseDNS=no')
    h2 = net.get('h2')
    h2.cmd('/usr/sbin/sshd -o UseDNS=no')
    h3 = net.get('h3')
    h3.cmd('/usr/sbin/sshd -o UseDNS=no')
    h4 = net.get('h4')
    h4.cmd('/usr/sbin/sshd -o UseDNS=no')
    h5 = net.get('h5')
    h5.cmd('/usr/sbin/sshd -o UseDNS=no')

    info( '*** Routing Table on Router:\n' )
    print net[ 'r0' ].cmd( 'route' )
    CLI( net )
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    run()
