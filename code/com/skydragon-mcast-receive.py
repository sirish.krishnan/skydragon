import socket
import struct
import sys
import rotatefile

MAXFILESIZE=1048576

rotfile = rotatefile.RotatingFile(max_file_size=MAXFILESIZE,filename='rcv')

multicast_group = ('224.3.29.71')
server_address = ('',12345)

# Create the socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind to the server address
sock.bind(server_address)

# Tell the operating system to add the socket to the multicast group
# on all interfaces.
group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

while True:
    print >>sys.stderr, '\nWaiting to receive message'
    data, address = sock.recvfrom(1024)
    print >>sys.stderr, 'received %s bytes from %s' % (len(data), address)
    print >>sys.stderr, data
    rotfile.write(data)
