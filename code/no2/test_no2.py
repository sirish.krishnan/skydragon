#import matplotlib.pyplot as plt
import time 
import RPi.GPIO as GPIO
# Import SPI library (for hardware SPI) and MCP3008 library.
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

SPI_PORT   = 0
SPI_DEVICE = 0
SLEEP_INTERVAL=60
nocl=0
cocl=1

f=open("log.txt",'w')
# Hardware SPI configuration:
mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
ro=2500

def getReading(channel):
    values = mcp.read_adc(channel)
    adcdata=values
    return adcdata

while True:      
    adcnox=getReading(nocl)
    rs=((100000*1023/(adcnox+1))-100000+820)
    ppm=pow((rs/(6.3*ro)),0.9862)
    print("adc={}; rs={} ; ppm ={}: ".format(adcnox,rs,ppm))
    time.sleep(SLEEP_INTERVAL)
    f.write(str(adcnox)+',')
    f.write(str(rs)+',')
    f.write(str(ppm)+',')
    f.write("\n")

f.close()
