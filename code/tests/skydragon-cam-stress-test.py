import socket
import struct
import sys
import time
import datetime
import rotatefile
import picamera

MAXFILESIZE=1048576
SLEEP_INTERVAL=1

rotfile = rotatefile.RotatingFile(max_file_size=MAXFILESIZE,filename='snd')

multicast_group = ('224.3.29.71', 12345)

# Create the datagram socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Set the time-to-live for messages to 1 so they do not go past the
# local network segment.
ttl = struct.pack('b', 10)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

# Instantiate a camera object
camera = picamera.PiCamera()

while True:
    # Send data to the multicast group
    now = datetime.datetime.now()
    message = 'SkyDragon;' + now.isoformat() + "\n"
    #message = 'SkyDragon;' + now.strftime("%Y-%m-%d:%H-%M-%S") + ';very important data\n'
    rotfile.write(message)
    print >>sys.stderr, 'sending "%s"' % message
    try:
        sent = sock.sendto(message, multicast_group)
    except:
        print "Failed to send multicast message"
    camera.resolution = (2592, 1944)
    camera.brightness = 50
    camera.vflip = True
    camera.annotate_text = message
    camera.capture(message + '.jpg')
    time.sleep(SLEEP_INTERVAL)
