import sys
import time
import datetime
import rotatefile
import picamera
import time

MAXFILESIZE=1048576
SLEEP_INTERVAL=60
MAX_VIDEO_TIMEOUT=60
MAX_VIDEO_SIZE=60
#VID_RES_WIDTH=1280
#VID_RES_HEIGHT=720
VID_RES_WIDTH=1920
VID_RES_HEIGHT=1080
IMG_RES_WIDTH=2592
IMG_RES_HEIGHT=1944
SNAPSHOT_MODE_DELAY=60*30
FRAMERATE=30

# Mode is set to video mode initially & then changed to 
# snapshot mode after expiry of MAX_VIDEO_TIMEOUT
mode = 0  
count = 0

rotfile = rotatefile.RotatingFile(max_file_size=MAXFILESIZE,filename='snd')

# Instantiate a camera object
camera = picamera.PiCamera()

start_time = datetime.datetime.now()

while True:
    # Send data to the multicast group
    print 'Mode is ' + str(mode)
    now = datetime.datetime.now()

    if (mode == 0):
        if ((datetime.datetime.now() - start_time).seconds >= SNAPSHOT_MODE_DELAY):
            mode = 1
            #mode = 0

    message = 'SkyDragon ' + now.strftime("%Y_%m_%d_%H_%M_%S")
    rotfile.write(message)
    camera.brightness = 50
    camera.vflip = True
    camera.annotate_text = message

    if (mode == 0):
        print 'SkyDragon : Capturing video for ' + str(MAX_VIDEO_SIZE) + ' seconds'
        camera.resolution = (VID_RES_WIDTH, VID_RES_HEIGHT)
        camera.framerate = FRAMERATE
        camera.start_recording(message+'.h264')
        start = datetime.datetime.now()
        while (datetime.datetime.now() - start).seconds < MAX_VIDEO_SIZE:
            camera.annotate_text =  'SkyDragon ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            camera.wait_recording(0.2)
        camera.stop_recording()
        camera.stop_preview()
        time.sleep(1)
    else:
        print 'SkyDragon : Capturing snapshot! ' 
        camera.exposure_mode = 'sports'
        camera.annotate_text =  'SkyDragon ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        camera.resolution = (IMG_RES_WIDTH, IMG_RES_HEIGHT)
        camera.capture(message + '.jpg')
        print 'Sleeping for some time...'
        time.sleep(SLEEP_INTERVAL)
