import socket
import RPi.GPIO as GPIO
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008
import subprocess
import Adafruit_BMP.BMP085 as BMP085 # Imports the BMP library
import struct
import sys
import time
import datetime
import rotatefile
import picamera
import re

def getReading(channel):
    values = mcp.read_adc(channel)
    adcdata=values
    return adcdata

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BOARD)
pin = 7
# Set pin as output
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(pin,GPIO.OUT)
GPIO.output(pin, False)
time.sleep(1)

NO2_SLEEP=60

SPI_PORT   = 0
SPI_DEVICE = 0
SLEEP_INTERVAL=1
nocl=0
cocl=1
count = NO2_SLEEP

MAXFILESIZE=1048576
SLEEP_INTERVAL1=1
SLEEP_INTERVAL2=2
MODE_DELAY=5*60

mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))
ro=2500

# Mode is set to vary the sleep interval dynamically
mode = 0  

rotfile = rotatefile.RotatingFile(max_file_size=MAXFILESIZE,filename='snd')

multicast_group = ('224.3.29.71', 12345)

# Create the datagram socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Set the time-to-live for messages to 1 so they do not go past the
# local network segment.
ttl = struct.pack('b', 10)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

sensor = BMP085.BMP085()

sleep_interval = SLEEP_INTERVAL1

start_time = datetime.datetime.now()

sum = 0.0

while True:

    if (mode == 0):
        if ((datetime.datetime.now() - start_time).seconds >= MODE_DELAY):
            sleep_interval = SLEEP_INTERVAL2

    # Read NO2 level
    ################
    if (count == NO2_SLEEP):
        sum = 0.0
        GPIO.output(pin, True)
        for i in range (0,5): 
            time.sleep(1)
            adcnox=getReading(nocl)
            rs=((100000*1023/(adcnox+1))-100000+820)
            ppm=pow((rs/(6.3*ro)),0.9862)
            sum = sum + ppm
            print("adc={}; rs={} ; ppm ={}: ".format(adcnox,rs,ppm))
        sum = sum / 5
        count = 0
        GPIO.output(pin, False)
    else:
        #sum = 0.0
        GPIO.output(pin, False)
        count = count + 1

    # Get BMP180 sensor data 
    ########################
    now = datetime.datetime.now()
    message = 'SkyDragon;' + now.isoformat() + \
                ';{0:0.2f} *C'.format(sensor.read_temperature()) + \
                ';{0:0.2f} m'.format(sensor.read_altitude()) + \
                ';{0:0.2f} Pa'.format(sensor.read_pressure()) + \
                ';{0:0.2f} Pa'.format(sensor.read_sealevel_pressure())

    # Get RSSI data
    ###############
    try:

        cmd = subprocess.Popen('iwconfig wlan0', shell=True,
                stdout=subprocess.PIPE)

        for line in cmd.stdout:
            if 'Link Quality' in line:
                wifi_raw_data = line.lstrip(' ')
                result = re.search('-.*dBm', wifi_raw_data)
                strength = result.group(0)
                result = re.search('[0-9]*/[0-9]*', wifi_raw_data)
                quality = result.group(0)
                rssi = ";" + strength + ";" + quality
            elif 'Not-Associated' in line:
                rssi = ';NA;NA' 

        message = message + rssi

    except:
        print "Failed to get link quality from iwconfig"

    # Add NO2 level to log
    ######################
    message = message + ";" + str(sum)

    # Send measured data back to the base station
    #############################################
    message = message + "\n"
    rotfile.write(message)
    print >>sys.stderr, 'sending "%s"' % message

    try:
        print "Sending message to the multicast group"
        sent = sock.sendto(message, multicast_group)
        print "Sent the message!"
    except:
        print "Failed to send multicast message"

    time.sleep(sleep_interval)
