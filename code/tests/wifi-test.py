import socket
import argparse
import subprocess
import struct
import sys
import time
import datetime
import rotatefile
#import picamera

MAXFILESIZE=1048576
SLEEP_INTERVAL=1

parser = argparse.ArgumentParser(description='SKYDRAGON - Balloon mapping program')
parser.add_argument(dest='interface', nargs='?', default='wlan0',
        help='wlan interface (default: wlan0)')
args = parser.parse_args()

print '\n---Press CTRL+Z or CTRL+C to stop.---\n'

rotfile = rotatefile.RotatingFile(max_file_size=MAXFILESIZE,filename='snd')

multicast_group = ('224.3.29.71', 12345)

# Create the datagram socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Set the time-to-live for messages to 1 so they do not go past the
# local network segment.
ttl = struct.pack('b', 10)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

# Instantiate a camera object
#camera = picamera.PiCamera()

while True:
    cmd = subprocess.Popen('iwconfig %s' % args.interface, shell=True,
            stdout=subprocess.PIPE)
    # Send data to the multicast group
    now = datetime.datetime.now()
    message = 'SkyDragon;' + now.isoformat() + "\n"
    #message = 'SkyDragon;' + now.strftime("%Y-%m-%d:%H-%M-%S") + ';very important data\n'
    #camera.resolution = (2592, 1944)
    #camera.brightness = 50
    #camera.vflip = True
    #camera.annotate_text = message
    #camera.capture(message + '.jpg')
    for line in cmd.stdout:
        if 'Link Quality' in line:
            message = message + line.lstrip(' ')
        elif 'Not-Associated' in line:
            message = message + 'No signal'
    try:
        sent = sock.sendto(message, multicast_group)
    except:
        print "Failed to send multicast message"
    print >>sys.stderr, 'sending "%s"' % message
    rotfile.write(message)
    time.sleep(SLEEP_INTERVAL)
